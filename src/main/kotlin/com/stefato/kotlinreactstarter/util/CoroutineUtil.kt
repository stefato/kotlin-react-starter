package com.stefato.kotlinreactstarter.util

import kotlin.coroutines.Continuation
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.coroutines.startCoroutine
import kotlin.js.Promise

fun <T> async(block: suspend () -> T): Promise<T> = Promise { resolve, reject ->
    block.startCoroutine(object : Continuation<T> {
        override fun resumeWith(result: Result<T>) {
            result.fold(resolve, reject)
        }

        override val context: CoroutineContext get() = EmptyCoroutineContext
    })
}