package com.stefato.kotlinreactstarter.components

import com.stefato.kotlinreactstarter.apiRoot
import com.stefato.kotlinreactstarter.model.Product
import com.stefato.kotlinreactstarter.util.HttpMethod
import com.stefato.kotlinreactstarter.util.async
import com.stefato.kotlinreactstarter.util.parseJsonResponse
import com.stefato.kotlinreactstarter.util.request
import kotlinx.html.InputType
import kotlinx.html.js.onChangeFunction
import kotlinx.html.js.onClickFunction
import kotlinx.serialization.DynamicObjectParser
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.w3c.dom.HTMLInputElement
import org.w3c.fetch.Response
import react.*
import react.dom.button
import react.dom.input
import kotlin.browser.sessionStorage


interface AddBarState : RState {
    var addProductName: String
    var addProductPrice: String
}


interface AddBarProps : RProps {
    var addFunction: (Product) -> Unit
}

class AddBar : RComponent<AddBarProps, AddBarState>() {

    override fun AddBarState.init() {
        addProductName = ""
        addProductPrice = ""
    }

    override fun RBuilder.render() {
        input(InputType.text) {
            attrs.value = state.addProductName
            attrs.onChangeFunction = {
                val target = it.target as HTMLInputElement
                setState { addProductName = target.value }
            }

        }
        input(InputType.number) {
            attrs.value = state.addProductPrice.toString()
            attrs.onChangeFunction = {
                val target = it.target as HTMLInputElement
                setState { addProductPrice = target.value }
            }
        }
        button {
            +"Add"
            attrs.onClickFunction = {
                addProduct()
                setState {
                    addProductName = ""
                    addProductPrice = ""
                }
            }
        }
    }

    private fun addProduct() {
        async {
            val token = sessionStorage.getItem("jwtToken")
            val addProductRequest = AddProductRequest(state.addProductName, state.addProductPrice.toDouble())
            val body = Json.stringify(AddProductRequest.serializer(), addProductRequest)
            val response = request(HttpMethod.POST, "$apiRoot/products", body, token)
            if (response.ok) {
                val newProduct = parseProductResponse(response)
                newProduct?.let {
                    props.addFunction(it)
                }
            }
        }
    }

    private suspend fun parseProductResponse(response: Response): Product? {
        val json = parseJsonResponse(response)
        return if (json == null) null else DynamicObjectParser().parse(json, Product.serializer())
    }
}


fun RBuilder.addProduct(addFunction: (Product) -> Unit) = child(AddBar::class) {
    attrs.addFunction = { addFunction(it) }
}

@Serializable
data class AddProductRequest(val name: String, val price: Double)