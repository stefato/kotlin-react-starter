package com.stefato.kotlinreactstarter.components

import com.stefato.kotlinreactstarter.apiRoot
import com.stefato.kotlinreactstarter.util.HttpMethod
import com.stefato.kotlinreactstarter.util.async
import com.stefato.kotlinreactstarter.util.parseJsonResponse
import com.stefato.kotlinreactstarter.util.request
import kotlinx.html.InputType
import kotlinx.html.js.onChangeFunction
import kotlinx.html.js.onClickFunction
import kotlinx.serialization.DynamicObjectParser
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.stringify
import org.w3c.dom.HTMLInputElement
import org.w3c.fetch.Response
import react.*
import react.dom.button
import react.dom.input
import kotlin.browser.sessionStorage

interface UserRegistrationState : RState {
    var pw: String
    var name: String
}

interface UserRegistrationProps : RProps {
    var loginFunction: () -> Unit
    var logoutFunction: () -> Unit
    var isLoggedIn: Boolean
}

class UserRegistration : RComponent<UserRegistrationProps, UserRegistrationState>() {

    override fun UserRegistrationState.init() {
        pw = ""
        name = ""
    }

    override fun RBuilder.render() {
        if (props.isLoggedIn) loggedInView()
        else loggedOutView()
    }

    private fun RBuilder.loggedInView() {
        button {
            +"Log Out"
            attrs {
                onClickFunction = {
                    logout()
                    props.logoutFunction()
                }
            }
        }
    }

    private fun RBuilder.loggedOutView() {
        input(InputType.text) {
            attrs {
                placeholder = "Name"
                value = state.name
                onChangeFunction = {
                    val target = it.target as HTMLInputElement
                    setState {
                        name = target.value
                    }
                }
            }
        }
        input(InputType.password) {
            attrs {
                placeholder = "Password"
                value = state.pw
                onChangeFunction = {
                    val target = it.target as HTMLInputElement
                    setState {
                        pw = target.value
                    }
                }
            }
        }
        button {
            +"Login"
            attrs {
                onClickFunction = {
                    login(state.name, state.pw)
                    setState {
                        name = ""
                        pw = ""
                    }
                }
            }
        }
    }


    private fun logout() {
        sessionStorage.removeItem("jwtToken")
    }

    private fun login(name: String, pw: String) {
        async {
            val authRequest = AuthRequest(name, pw)
            val body = Json.stringify(AuthRequest.serializer(),authRequest)
            val response = request(HttpMethod.POST, "$apiRoot/authorize", body)
            if (response.ok) {
                val authResponse = parseLoginResponse(response)
                authResponse?.let {
                    sessionStorage.setItem("jwtToken", it.token)
                    props.loginFunction()
                }
            }
        }
    }

    private suspend fun parseLoginResponse(response: Response): AuthResponse? {
        val json = parseJsonResponse(response)
        return if (json == null) null else DynamicObjectParser().parse(json, AuthResponse.serializer())
    }


}

fun RBuilder.userRegistration(isLoggedIn: Boolean, loginFunction: () -> Unit, logoutFunction: () -> Unit) = child(UserRegistration::class) {
    attrs.isLoggedIn = isLoggedIn
    attrs.loginFunction = loginFunction
    attrs.logoutFunction = logoutFunction
}

@Serializable
data class AuthRequest(val name: String, val password: String)

@Serializable
data class AuthResponse(val token: String)
