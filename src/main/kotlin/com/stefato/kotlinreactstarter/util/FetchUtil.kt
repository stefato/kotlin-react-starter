package com.stefato.kotlinreactstarter.util

import kotlinx.coroutines.await
import org.w3c.fetch.Headers
import org.w3c.fetch.Request
import org.w3c.fetch.RequestInit
import org.w3c.fetch.Response
import kotlin.browser.window

enum class HttpMethod {
    GET,
    POST,
    PUT,
    DELETE
}

suspend fun request(method: HttpMethod, url: String, body: String? = null, token: String? = null): Response {
    val headers = Headers().apply {
        if (token != null) append("Authorization", "Bearer $token")
        if (body != null) append("Content-Type", "application/json")
    }

    val reqInit = RequestInit(method.name, headers, body)
    return window.fetch(Request(url, reqInit)).await()
}

suspend fun parseJsonResponse(response: Response): Any? {
    val json: dynamic = response.json().await()
    if (json.error != null) {
        return null
    }
    return json as Any
}

