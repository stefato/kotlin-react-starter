package com.stefato.kotlinreactstarter.components


import com.stefato.kotlinreactstarter.apiRoot
import com.stefato.kotlinreactstarter.model.Product
import com.stefato.kotlinreactstarter.util.HttpMethod
import com.stefato.kotlinreactstarter.util.async
import com.stefato.kotlinreactstarter.util.parseJsonResponse
import com.stefato.kotlinreactstarter.util.request
import kotlinx.serialization.DynamicObjectParser
import kotlinx.serialization.list
import org.w3c.fetch.Response
import react.*
import react.dom.*

interface ProductTableState : RState {
    var products: List<Product>
}

interface ProductTableProps : RProps {
    var isLoggedIn: Boolean
}

class ProductTable : RComponent<ProductTableProps, ProductTableState>() {

    override fun ProductTableState.init() {
        products = listOf()
    }

    override fun componentDidMount() {
        fetchProducts()
    }

    override fun RBuilder.render() {
        table()
        if (props.isLoggedIn) addProduct { addProductToTable(it) }
    }

    private fun RBuilder.table() {
        table {
            thead {
                tr {
                    th { +"Name" }
                    th { +"Price" }
                }
            }
            tbody {
                state.products.forEach { product ->
                    tr {
                        td { +product.name }
                        td { +product.price.toString() }
                        if (props.isLoggedIn) td { editProduct(product) { fetchProducts() } }
                    }
                }
            }
        }
    }

    private fun addProductToTable(product: Product) {
        val newProducts = state.products.toMutableList()
        newProducts.add(product)
        setState {
            products = newProducts
        }
    }

    private fun fetchProducts() {
        async {
            val response = request(HttpMethod.GET, "$apiRoot/products")
            if (response.ok) {
                val productList = parseProductsResponse(response)
                setState { products = productList.toMutableList() }
            }
        }
    }

    private suspend fun parseProductsResponse(response: Response): List<Product> {
        val json = parseJsonResponse(response)
        return if (json == null) emptyList() else DynamicObjectParser().parse(json, Product.serializer().list)
    }
}

fun RBuilder.productList(isLoggedIn: Boolean) = child(ProductTable::class) {
    attrs.isLoggedIn = isLoggedIn
}



