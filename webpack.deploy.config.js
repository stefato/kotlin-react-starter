const webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const path = require("path");

const dist = path.resolve(__dirname, "build/dist");

module.exports = {
mode: 'production',
    entry: {
        main: "main"
    },
    output: {
        filename: "[name].bundle.js",
        path: dist,
        publicPath: ""
    },
    module: {
        rules: [{
            test: /\.css$/,
            use: [
                'style-loader',
                'css-loader'
            ]
        }]
    },
    resolve: {
        modules: [
            path.resolve(__dirname, "build/kotlin-js-min/main"),
            path.resolve(__dirname, "node_modules/"),
            path.resolve(__dirname, "src/main/web/")
        ]
    },
    optimization: {
        minimizer: [new UglifyJsPlugin()],
      },
    plugins: [
        new HtmlWebpackPlugin({
                    template: 'src/main/web/index.html'
                }),
         new webpack.DefinePlugin({
              'process.env': {
                'NODE_ENV': JSON.stringify('production')
              }
            }),
            new webpack.optimize.AggressiveMergingPlugin(),
            new CompressionPlugin({
                  asset: "[path].gz[query]",
                  algorithm: "gzip",
                  test: /\.js$|\.css$|\.html$/,
                  threshold: 10240,
                  minRatio: 0.8
                }),
    ]
};
