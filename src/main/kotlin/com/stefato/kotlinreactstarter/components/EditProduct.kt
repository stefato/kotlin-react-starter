package com.stefato.kotlinreactstarter.components

import com.stefato.kotlinreactstarter.apiRoot
import com.stefato.kotlinreactstarter.model.Product
import com.stefato.kotlinreactstarter.util.HttpMethod
import com.stefato.kotlinreactstarter.util.async
import com.stefato.kotlinreactstarter.util.request
import kotlinx.html.InputType
import kotlinx.html.js.onChangeFunction
import kotlinx.html.js.onClickFunction
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.w3c.dom.HTMLInputElement
import react.*
import react.dom.button
import react.dom.input
import kotlin.browser.sessionStorage

interface EditBarState : RState {
    var editProductName: String
    var editProductPrice: String
    var viewMode: Boolean
}

interface EditBarProps : RProps {
    var product: Product
    var fetchFunction: () -> Unit
}

class EditBar : RComponent<EditBarProps, EditBarState>() {

    override fun EditBarState.init() {
        EditBar::class.js.asDynamic().getDerivedStateFromProps = { p: EditBarProps, s: EditBarState ->
            editProductName = p.product.name
            editProductPrice = p.product.price.toString()
            viewMode = false
        }
    }

    override fun RBuilder.render() {
        button {
            +"Delete"
            attrs.onClickFunction = { deleteProduct(props.product) }
        }

        if (state.viewMode) renderInputFields()
        else renderEditButton()
    }

    private fun RBuilder.renderEditButton() {
        button {
            +"Edit"
            attrs.onClickFunction = { setState { viewMode = true } }
        }
    }

    private fun RBuilder.renderInputFields() {
        input(InputType.text) {
            attrs.value = state.editProductName
            attrs.onChangeFunction = {
                val target = it.target as HTMLInputElement
                setState {
                    editProductName = target.value
                }
            }
        }
        input(InputType.number) {
            attrs.value = state.editProductPrice
            attrs.onChangeFunction = {
                val target = it.target as HTMLInputElement
                setState {
                    editProductPrice = target.value
                }
            }
        }
        button {
            +"Save"
            attrs.onClickFunction = {
                updateProduct(props.product)
                setState { viewMode = false }
            }
        }
    }

    private fun deleteProduct(product: Product) {
        async {
            val token = sessionStorage.getItem("jwtToken")
            val response = request(HttpMethod.DELETE, "$apiRoot/products/${product.id}", token = token)
            if (!response.ok) {
                //error handling
            }
        }.then {
            //TODO just update client side
            props.fetchFunction()
        }
    }

    private fun updateProduct(product: Product) {

        val inputBlank = state.editProductName.isBlank() || state.editProductPrice.isBlank()
        val inputNotChanged = !(state.editProductName != product.name || state.editProductPrice != product.price.toString())

        //TODO error msg if input is blank
        if (inputBlank || inputNotChanged) return

        async {
            val token = sessionStorage.getItem("jwtToken")
            val updateProductRequest = UpdateProductRequest(state.editProductName,state.editProductPrice.toDouble())
            val body = Json.stringify(UpdateProductRequest.serializer(),updateProductRequest)
            val response = request(HttpMethod.PUT, "$apiRoot/products/${product.id}", body, token)
            if (!response.ok) {
                //error handling
            }
        }.then {
            //TODO just update client side
            props.fetchFunction()
        }
    }

}

@Serializable
data class UpdateProductRequest(val name: String, val price: Double)

fun RBuilder.editProduct(product: Product, fetchFunction: () -> Unit) = child(EditBar::class) {
    attrs.product = product
    attrs.fetchFunction = fetchFunction
}