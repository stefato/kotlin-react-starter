const webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const path = require("path");

const dist = path.resolve(__dirname, "build/dist");

module.exports = {
    mode: 'development',
    entry: {
        main: "main"
    },
    output: {
        pathinfo: true,
        filename: "[name].[chunkhash].bundle.js",
        path: dist,
        publicPath: ""
    },
    watch: true,
    module: {
        rules: [{
            test: /\.css$/,
            use: [
                'style-loader',
                'css-loader'
            ]
        }]
    },
    resolve: {
        modules: [
            path.resolve(__dirname, "build/node_modules/"),
            path.resolve(__dirname, "node_modules/"),
            path.resolve(__dirname, "src/main/web/")
        ]
    },
     optimization: {
           splitChunks:{
           chunks: 'all',
           name: false
           }
          },
    devtool: 'cheap-source-map',
    plugins: [
        new HtmlWebpackPlugin({
            template: 'src/main/web/index.html',
            chunksSortMode: 'manual',
            minify: {
                removeAttributeQuotes: false,
                collapseWhitespace: false,
                html5: false,
                minifyCSS: false,
                minifyJS: false,
                minifyURLs: false,
                removeComments: false,
                removeEmptyAttributes: false
            },
            hash: false
        }),
        new BrowserSyncPlugin({
            host: 'localhost',
            port: 8090,
            server: {
                baseDir: ['./build/dist']
            }
        })
    ]
};
