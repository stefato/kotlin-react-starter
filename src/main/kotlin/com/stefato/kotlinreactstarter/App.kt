package com.stefato.kotlinreactstarter

import com.stefato.kotlinreactstarter.components.productList
import com.stefato.kotlinreactstarter.components.userRegistration
import react.*
import kotlin.browser.sessionStorage

interface AppState : RState {
    var isLoggedIn: Boolean
}

class App : RComponent<RProps, AppState>() {
    override fun AppState.init() {
        isLoggedIn = false
    }

    override fun RBuilder.render() {
        userRegistration(state.isLoggedIn,{ login() }, { logout() })
        productList(state.isLoggedIn)
    }

    override fun componentDidMount() {
        val token = sessionStorage.getItem("jwtToken")
        setState { isLoggedIn = !token.isNullOrEmpty() }
    }

    private fun login() {
        setState { isLoggedIn = true }
    }

    private fun logout() {
        setState { isLoggedIn = false }
    }
}

fun RBuilder.app() = child(App::class) {}
